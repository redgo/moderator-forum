﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MVCForum.Utilities
{
    public class ImageUtils
    {
        public bool isImageToBig(string imageUrl)
        {
            Image image = getImageFromUrl(imageUrl);
            if (image.Width > 400 || image.Height > 400)
            {
                return true;
            }
            else
            {
                return false;
            }     
        }

        public Image getImageFromUrl(string imageUrl)
        {
            System.IO.Stream stream = null;
            HttpWebRequest wReq = (HttpWebRequest)WebRequest.Create(imageUrl);
            HttpWebResponse wRes = (HttpWebResponse)(wReq).GetResponse();
            stream = wRes.GetResponseStream();
            var image = Image.FromStream(stream);
            return image;
        }

        public int getImageSizeFromUrl(string imageUrl)
        {
            System.Net.WebClient client = new System.Net.WebClient();
            client.OpenRead(imageUrl);
            int bytesTotal = Convert.ToInt32(client.ResponseHeaders["Content-Length"]);
            System.Net.WebClient myWebClient = new System.Net.WebClient();
            return bytesTotal;
        }
    }
}
