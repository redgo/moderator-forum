﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MVCForum.Domain.DomainModel;

namespace MVCForum.Website.Areas.Moderator.ViewModels
{
    public class ListRulesViewModel
    {
        public IEnumerable<Rule> Rules { get; set; }
    }

    public class CreateRuleViewModel
    {
        [HiddenInput]
        public Guid Id { get; set; }

        [DisplayName("Rule Name")]
        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        [DisplayName("Rule EditMessage")]
        [DataType(DataType.MultilineText)]
        public string EditMessage { get; set; }

        [DisplayName("Action needs confirmation?")]
        public bool NeedConfirmation { get; set; }

        [DisplayName("Action result")]
        public Result Result { get; set; }
        
        [DisplayName("Basic rules")]
        public List<ActionsToMade> ActionsToMade { get; set; }

        [DisplayName("Penalty")]
        public Penalty Penalty { get; set; }


        public List<Rule> AllRules { get; set; }
    }

    public class EditRuleViewModel
    {
        [DisplayName("Rule Name")]
        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        [DisplayName("Rule EditMessage")]
        [DataType(DataType.MultilineText)]
        public string EditMessage { get; set; }

        [DisplayName("Action needs confirmation?")]
        public bool NeedConfirmation { get; set; }

        [DisplayName("Action result")]
        public Result Result { get; set; }

        [DisplayName("Basic rules")]
        public List<ActionsToMade> ActionsToMade { get; set; }

        [DisplayName("Penalty")]
        public Penalty Penalty { get; set; }

        [HiddenInput]
        public Guid Id { get; set; }

        public List<Rule> AllRules { get; set; }
    }

    public class DeleteRuleViewModel
    {
        [HiddenInput]
        public Guid Id { get; set; }

        public Rule Rule { get; set; }
    }
}