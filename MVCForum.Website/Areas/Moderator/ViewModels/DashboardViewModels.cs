﻿using System.Collections.Generic;
using System.Web.Mvc;
using MVCForum.Domain.DomainModel;

namespace MVCForum.Website.Areas.Moderator.ViewModels
{
    public class PostsToModerateViewModel
    {
        public IEnumerable<Post> Posts { get; set; }
        public Post PostToModerate { get; set; }
    }
}