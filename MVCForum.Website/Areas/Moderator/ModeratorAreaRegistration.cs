﻿using System.Web.Mvc;
using LowercaseRoutesMVC;

namespace MVCForum.Website.Areas.Moderator
{
    public class ModeratorAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Moderator";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRouteLowercase(
                "Moderator_editcategoryroute",
                "Moderator/{controller}/{action}/{id}",
                new { controller = "ModeratorCategory", action = "Index", id = UrlParameter.Optional }
            );
            context.MapRouteLowercase(
                "Moderator_edituserroute",
                "Moderator/{controller}/{action}/{userId}",
                new { controller = "Moderator", action = "Index", userId = UrlParameter.Optional }
            );
            context.MapRouteLowercase(
                "Moderator_defaultroute",
                "Moderator/{controller}/{action}/{id}",
                new { controller = "Moderator", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
