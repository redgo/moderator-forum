﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MVCForum.Domain.Constants;
using MVCForum.Domain.DomainModel;
using MVCForum.Domain.Interfaces.Services;
using MVCForum.Domain.Interfaces.UnitOfWork;
using MVCForum.ForumModerator;
using MVCForum.Website.Areas.Admin.ViewModels;
using MVCForum.Website.Areas.Moderator.ViewModels;
using MVCForum.Data.Context;
using MVCForum.Services;
using MVCForum.Data.Repositories;


namespace MVCForum.Website.Areas.Moderator.Controllers
{
    [Authorize(Roles = AppConstants.ModeratorRoleName)]
    public class ModeratorRuleController : BaseModeratorController
    {
        private readonly RuleService _ruleService;
		private readonly PrivateMessageService _messageService;
		private readonly MVCForumContext _context;
		private readonly MembershipRepository _membershipRepository;
		private readonly PrivateMessageRepository _privateMessageRepository;

        public ModeratorRuleController(ILoggingService loggingService,
                                        IUnitOfWorkManager unitOfWorkManager,
                                       IMembershipService membershipService,
                                       ILocalizationService localizationService,
                                       ISettingsService settingsService)
            : base(loggingService, unitOfWorkManager, membershipService, localizationService, settingsService)
        {
            _ruleService = new RuleService();
			_context = new MVCForumContext();
			_membershipRepository = new MembershipRepository(_context);
			_privateMessageRepository = new PrivateMessageRepository(_context);
			_messageService = new PrivateMessageService(_privateMessageRepository, _membershipRepository);
        }

        public ActionResult Index()
        {
            return View();
        }

        [ChildActionOnly]
        public PartialViewResult ListAllRules(Guid id)
        {
            using (UnitOfWorkManager.NewUnitOfWork())
            {
                var viewModel = new ListRulesViewModel
                                    {
                                        Rules = _ruleService.GetAll()
                                    };
                return PartialView(viewModel);
            }
        }

        [ChildActionOnly]
        public PartialViewResult GetMainRules()
        {
            using (UnitOfWorkManager.NewUnitOfWork())
            {
                var viewModel = new ListRulesViewModel
                                    {
                                        Rules = _ruleService.GetAll()
                                    };
                return PartialView(viewModel);
            }
        }

        [ChildActionOnly]
        public PartialViewResult CreateRule()
        {
            using (UnitOfWorkManager.NewUnitOfWork())
            {
                var actionsToMade = GetBasicRulesList();
                var ruleViewModel = new CreateRuleViewModel
                {
                    AllRules = _ruleService.GetAll().ToList(),
                    ActionsToMade = actionsToMade
                };
                return PartialView(ruleViewModel);
            }
        }

        private static List<ActionsToMade> GetBasicRulesList()
        {
            var actionsToMade = new List<ActionsToMade>
            {
                new ActionsToMade() { Key = BasicRule.RemoveWhitespaces, Value = false },
                new ActionsToMade() { Key = BasicRule.Curses, Value = false },
                new ActionsToMade() { Key = BasicRule.Pictures, Value = false },
                new ActionsToMade() { Key = BasicRule.Spam, Value = false },
                new ActionsToMade() { Key = BasicRule.Spelling, Value = false }
            };
            return actionsToMade;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateRule(CreateRuleViewModel ruleViewModel)
        {
            if (ModelState.IsValid)
            {
                using (var unitOfWork = UnitOfWorkManager.NewUnitOfWork())
                {
                    try
                    {
                        var rule = new Rule
                                           {
                                               Name = ruleViewModel.Name,
                                               EditMessage = ruleViewModel.EditMessage,
                                               ActionsToMade = ruleViewModel.ActionsToMade,
                                               Id = Guid.NewGuid(),
                                               NeedConfirmation = ruleViewModel.NeedConfirmation,
                                               Penalty = ruleViewModel.Penalty,
                                               Result = ruleViewModel.Result
                                           };
                       
                        
                        _ruleService.Add(rule);

                        // We use temp data because we are doing a redirect
                        TempData[AppConstants.MessageViewBagName] = new GenericMessageViewModel
                                                                        {
                                                                            Message = "Rule Created",
                                                                            MessageType =
                                                                                GenericMessages.success
                                                                        };
                        unitOfWork.Commit();

						NotifyMembers();
                    }
                    catch (Exception)
                    {
                        TempData[AppConstants.MessageViewBagName] = new GenericMessageViewModel
                        {
                            Message = "Error",
                            MessageType = GenericMessages.error
                        };

                        unitOfWork.Rollback();
                    }
                }
            }
            
            return RedirectToAction("Index");
        }
    

    public ActionResult EditRule(Guid id)
        {
            using (UnitOfWorkManager.NewUnitOfWork())
            {
                var rule = _ruleService.Get(id);
                var categoryViewModel = new EditRuleViewModel
                                            {
                                                Name = rule.Name,
                                                EditMessage = String.Empty,
                                                Id = rule.Id,
                                                NeedConfirmation = rule.NeedConfirmation,
                                                Penalty = rule.Penalty,
                                                Result = rule.Result,
                                                ActionsToMade = rule.ActionsToMade,
                                                AllRules = _ruleService.GetAll()
                                                    .Where(x => x.Id != rule.Id)
                                                    .ToList(),
                                            };

                return View(categoryViewModel);
            }
        }

        [HttpPost]
        public ActionResult EditRule(EditRuleViewModel ruleViewModel)
        {
            if (ModelState.IsValid)
            {
                using (var unitOfWork = UnitOfWorkManager.NewUnitOfWork())
                {
                    try
                    {
                        var rule = _ruleService.Get(ruleViewModel.Id);

                        rule.EditMessage = ruleViewModel.EditMessage;
                        rule.Name = ruleViewModel.Name;
                        rule.ActionsToMade = ruleViewModel.ActionsToMade;
                        rule.Penalty = ruleViewModel.Penalty;
                        rule.NeedConfirmation = ruleViewModel.NeedConfirmation;
                        rule.Result = ruleViewModel.Result;

                        _ruleService.UpdateSlugFromName(rule);

                        unitOfWork.Commit();

						

						NotifyMembers();
                    }
                    catch (Exception ex)
                    {
                        LoggingService.Error(ex);
                        unitOfWork.Rollback();
                    }
                }
            }

            return View("PopupConfirm");
        }

        public ActionResult DeleteRuleConfirmation(Guid id)
        {
            using (UnitOfWorkManager.NewUnitOfWork())
            {
                var rule = _ruleService.Get(id);
                var viewModel = new DeleteRuleViewModel
                                    {
                                        Id = rule.Id,
                                        Rule = rule
                                    };

            return View(viewModel);
            }
        }

        public ActionResult DeleteRule(Guid id)
        {
            using (var unitOfWork = UnitOfWorkManager.NewUnitOfWork())
            {
                try
                {
                    var rule = _ruleService.Get(id);
                    _ruleService.Delete(rule);
                    TempData[AppConstants.MessageViewBagName] = new GenericMessageViewModel
                                                                    {
                                                                        Message = "Rule Deleted",
                                                                        MessageType = GenericMessages.success
                                                                    };
                    unitOfWork.Commit();
                }
                catch (Exception)
                {
                    unitOfWork.Rollback();
                }

                return RedirectToAction("Index");
            }
        }

		private void NotifyMembers(string userName = "")
		{
			var usersToNotify = _context.MembershipUser.Where(p => p.UserName != "admin").ToList();

			var privateMessage = new PrivateMessage()
			{
				UserFrom = _context.MembershipUser.Where(p => p.UserName == "moderator").FirstOrDefault(),
				Subject = "Zmiana reguł korzystania z forum",
				DateSent = DateTime.Now
			};

			foreach (var rule in _ruleService.Load().ToList())
			{
				privateMessage.Message += rule.Name + "; Penalty: " + rule.Result.ToString() + "\n";
			}
				
			foreach (var user in usersToNotify)
			{
				privateMessage.UserTo = user;
				_privateMessageRepository.Add(privateMessage);
			}

			_context.SaveChanges();
		}
    }
}
