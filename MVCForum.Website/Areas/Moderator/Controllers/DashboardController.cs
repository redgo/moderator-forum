﻿using System;
using System.Web.Mvc;
using MVCForum.Domain.Constants;
using MVCForum.Domain.Interfaces.Services;
using MVCForum.Domain.Interfaces.UnitOfWork;
using MVCForum.Website.Areas.Admin.ViewModels;
using MVCForum.Website.Areas.Moderator.ViewModels;

namespace MVCForum.Website.Areas.Moderator.Controllers
{
    [Authorize(Roles = AppConstants.ModeratorRoleName)]
    public class DashboardController : BaseModeratorController
    {
        private readonly IPostService _postService;
        private readonly ITopicService _topicService;
        private readonly ITopicTagService _topicTagService;
        private readonly IMembershipUserPointsService _membershipUserPointsService;
        const int AmountToShow = 7;

        public DashboardController(ILoggingService loggingService, IUnitOfWorkManager unitOfWorkManager, IMembershipService membershipService,
            ILocalizationService localizationService, ISettingsService settingsService, IPostService postService, 
            ITopicService topicService, ITopicTagService topicTagService, IMembershipUserPointsService membershipUserPointsService)
            : base(loggingService, unitOfWorkManager, membershipService, localizationService, settingsService)
        {
            _membershipUserPointsService = membershipUserPointsService;
            _postService = postService;
            _topicService = topicService;
            _topicTagService = topicTagService;
        }

        [HttpPost]
        public PartialViewResult PostsToModerate()
        {
            if (Request.IsAjaxRequest())
            {
                using (UnitOfWorkManager.NewUnitOfWork())
                {
                    return PartialView(new PostsToModerateViewModel { Posts = _postService.GetAllPostToModerate() });
                }
            }
            return null;
        }

        [HttpPost]
        public ActionResult ModeratePost(Guid Id)
        {
            using (var unitOfWork = UnitOfWorkManager.NewUnitOfWork())
            {
                _postService.SetModerateFlag(_postService.Get(Id), false);
                unitOfWork.Commit();
                TempData[AppConstants.MessageViewBagName] = new GenericMessageViewModel
                {
                    Message = "Post was edited successfully",
                    MessageType = GenericMessages.success
                };
                return RedirectToAction("Index", "Moderator");
            }
        }
    }
}
