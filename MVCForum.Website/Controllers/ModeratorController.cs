﻿using System.Web.Mvc;
using MVCForum.Domain.Constants;
using MVCForum.Domain.Interfaces.Services;
using MVCForum.Domain.Interfaces.UnitOfWork;
using MVCForum.Website.Controllers;

namespace MVCForum.Website.Controllers
{
    [Authorize(Roles = AppConstants.ModeratorRoleName)]
    public class ModeratorController : BaseModeratorController
    {
        public ModeratorController(ILoggingService loggingService, 
            IUnitOfWorkManager unitOfWorkManager, 
            IMembershipService membershipService, 
            ILocalizationService localizationService, 
            ISettingsService settingsService) 
            : base(loggingService, unitOfWorkManager, membershipService, localizationService, settingsService)
        {
        }
        
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }
	}
}