ALTER TABLE Settings
ADD NewMemberEmailConfirmation bit NULL
GO
ALTER TABLE Post
ADD IpAddress nvarchar(50) NULL
GO
ALTER TABLE MembershipUser
	ADD Warnings int NOT NULL DEFAULT 0
GO
ALTER TABLE MembershipUser
	ADD IsBanned bit NOT NULL DEFAULT 0
GO
