﻿using MVCForum.Domain.DomainModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCForum.Data.Mapping
{
	public class MembershipUserHistoryMapping : EntityTypeConfiguration<MembershipUserHistory>
	{
		public MembershipUserHistoryMapping()
		{
			HasKey(x => x.Id);
		}
	}
}
