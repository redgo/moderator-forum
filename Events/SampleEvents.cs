﻿using System.Collections.Generic;
using MVCForum.Domain.DomainModel;
using MVCForum.Domain.Events;
using MVCForum.Domain.Interfaces.Events;
using MVCForum.ForumModerator;
using MVCForum.Utilities;

namespace Events
{
	public class SampleEvents : IEventHandler
	{
		RuleService ruleService = new RuleService();
		public void RegisterHandlers(EventManager theEventManager)
		{
			theEventManager.AfterPostMade += AfterPostMadeHandler;
			theEventManager.AfterPostEdit += AfterPostEditHandler;
		}

		private void AfterPostEditHandler(object sender, PostEditedEventArgs e)
		{
			List<Rule> usedRules = ruleService.Load();

			foreach (var usedRule in usedRules)
			{
				if (usedRule.NeedConfirmation.Equals(false) ||
					usedRule.NeedConfirmation.Equals(true) && e.Post.ToModerate == false)
				{
					foreach (var action in usedRule.ActionsToMade)
					{
						if (action.Value.Equals(true))
						{
							bool makeAction = ApplyRules(e, action);
							if (makeAction == true && usedRule.Result == Result.Remove)
							{
								Moderation.DeletePost(e.Post);
							}
						}
					}
				}
			}
		}

		public static void SetToModerateFlag(Post post, bool needConfirmation, bool update)
		{
			Moderation.SetFlag(post, needConfirmation, update);
		}


		private void AfterPostMadeHandler(object sender, PostMadeEventArgs e)
		{
			List<Rule> usedRules = ruleService.Load();

			foreach (var usedRule in usedRules)
			{
				SetToModerateFlag(e.Post, usedRule.NeedConfirmation, false);

				if (usedRule.NeedConfirmation.Equals(false))
				{
					foreach (var action in usedRule.ActionsToMade)
					{
						if (action.Value.Equals(true))
						{
							bool makeAction = ApplyRules(e, action);
							if (makeAction == true && usedRule.Result == Result.Remove)
							{
								Moderation.DeletePost(e.Post);
							}
						}
					}
				}
			}
		}

		private static bool ApplyRules(PostEditedEventArgs e, ActionsToMade action)
		{
			if (action.Value == true)
			{
				return PerformAction(e.Post, action);
			}
			return false;
		}
		private static bool ApplyRules(PostMadeEventArgs e, ActionsToMade action)
		{
			if (action.Value == true)
			{
				return PerformAction(e.Post, action);
			}
			return false;
		}

		public static bool PerformAction(Post post, ActionsToMade action)
		{
			switch (action.Key)
			{
				case BasicRule.Curses:
					var clearBadWords = new ClearBadWords(post);
					if (clearBadWords.IsPostValid().Equals(true))
					{
						return false;
					}
					else
					{
						clearBadWords.CorrectPost();
						Penalties.PunishHim(post.User.Id, 4, 2);
						Penalties.SendUserPunishmentNotification(post.User.Id, 4);
						return true;
					}
				case BasicRule.Pictures:
					bool isTooBig = false;
					StringUtils.NormalizeImgSize(post.PostContent, ref isTooBig);
					//Penalties.PunishHim(post.User.Id, 1, 3);
					//Penalties.SendUserPunishmentNotification(post.User.Id, 1);
					return isTooBig;
				case BasicRule.RemoveWhitespaces:
					var redundantWhiteSpaces = new RedundantWhiteSpaces(post);
					if (redundantWhiteSpaces.isValid().Equals(true))
					{
						return false;
					}
					else
					{
						redundantWhiteSpaces.correctPost();
						Penalties.PunishHim(post.User.Id, 2, 1);
						Penalties.SendUserPunishmentNotification(post.User.Id, 2);
						return true;
					}
				case BasicRule.Spam:
					var spam = new SpamDetector();
					var postWithSpam = spam.DetectIfPostHaveSpam(post);
					if (postWithSpam)
					{
						SetToModerateFlag(post, true, false);
						//Penalties.PunishHim(post.User.Id, 1, 3);
						//Penalties.SendUserPunishmentNotification(post.User.Id, 1);
						return true;
					}						
					else
						return false;
				case BasicRule.Spelling:
					var spelling = new SpellingCorrection(post);
					if (spelling.isValid().Equals(true))
					{
						return false;
					}
					else
					{
						spelling.correctPost();
						Penalties.PunishHim(post.User.Id, 1, 1);
						Penalties.SendUserPunishmentNotification(post.User.Id, 1);
						return true;
					}

				default:
					return false;
			}
		}
	}
}
