﻿using MVCForum.Data.Context;
using MVCForum.Data.Repositories;
using MVCForum.Domain.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCForum.ForumModerator
{
    public static class Penalties
    {
		private static MVCForumContext _context = new MVCForumContext();
		public static void PunishHim(Guid userId, int penaltyValue, int penaltyMultiplier)
		{
			var membershipRepository = new MembershipRepository(_context);
			membershipRepository.AddPenaltyPoints(userId, penaltyValue, penaltyMultiplier);
			_context.SaveChanges();
		}

		public static void SendUserPunishmentNotification(Guid userId, int points)
		{
			var userToNotify = _context.MembershipUser.Where(p => p.Id == userId).FirstOrDefault();

			var privateMessage = new PrivateMessage()
			{
				UserFrom = _context.MembershipUser.Where(p => p.UserName == "moderator").FirstOrDefault(),
				Subject = "Warning recieved!",
				DateSent = DateTime.Now,
				UserTo = userToNotify
			};

			privateMessage.Message = "You have recieved a warning worth " + points.ToString() + " points!\n"
				+ "Now you have " + userToNotify.Warnings.ToString() + "warning points!\nAfter passing 30 warning points, you will recieve a account ban";

			PrivateMessageRepository pmr = new PrivateMessageRepository(_context);
			pmr.Add(privateMessage);
			_context.SaveChanges();
		}

        public static int penaltyMultiplier(int numberOfWarnings, int penaltyMultiplier)
        {
			return numberOfWarnings * penaltyMultiplier;
        }
        public static bool wasBannedLastTime(MembershipUser user)
        {
            double elapsedDays = (user.LastLockoutDate - DateTime.Now).TotalDays;
            if (elapsedDays < 30)
            {
                return true;
            }
            return false;
        }
    }
}
