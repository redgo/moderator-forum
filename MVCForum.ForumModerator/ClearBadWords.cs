﻿using MVCForum.Data.Context;
using MVCForum.Data.Repositories;
using MVCForum.Domain.DomainModel;
using MVCForum.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace MVCForum.ForumModerator
{
    public class ClearBadWords
    {
		private IList<BannedWord> _bannedWords = new List<BannedWord>();
        public Post post { get; set; }

        public ClearBadWords(Post post)
        {
			using (var context = new MVCForumContext())
			{
				var bannedWordRepository = new BannedWordRepository(context);
				_bannedWords = bannedWordRepository.GetAll();
			}
			
            this.post = post;
        }

		public bool IsPostValid()
		{
			bool result = true;

			using (var context = new MVCForumContext())
			{
				foreach (var bannedWord in _bannedWords)
				{
					if (post.PostContent.ToLower().Contains(bannedWord.Word))
						result = false;
				}
			}
			
			return result;
		}

        public void CorrectPost()
        {
            using (var context = new MVCForumContext())
            {
                CensorUtils utils = new CensorUtils(_bannedWords.Select(x => x.Word).ToList());
                post.PostContent = utils.CensorText(this.post.PostContent);
            }
        }
    }
}
