﻿using System.Net.Configuration;
using MVCForum.Data.Context;
using MVCForum.Data.Repositories;
using MVCForum.Data.UnitOfWork;
using MVCForum.Domain.DomainModel;

namespace MVCForum.ForumModerator
{
    public static class Moderation
    {
        public static void SetFlag(Post post, bool needConfirmation, bool update)
        {
            using (var context = new MVCForumContext())
            {
                var postRepository = new PostRepository(context);
                postRepository.SetModerateFlag(post, needConfirmation, update);
            }
        }

        public static void DeletePost(Post post)
        {
            using (var context = new MVCForumContext())
            {
                var postRepository = new PostRepository(context);
                postRepository.Delete(post);
            }
        }
    }
}
