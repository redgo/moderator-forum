﻿using MVCForum.Domain.DomainModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace MVCForum.ForumModerator
{
    public class SpellingCorrection
    {
        private Post post;

        public SpellingCorrection(Post post)
        {
            this.post = post;
        }


        public bool isValid()
        {
            Regex wordRegex = new Regex("[a-z]+");
            string word;

            List<string> words = post.PostContent.Split(' ').ToList();

            foreach (var w in words)
            {
                word = w.Trim().ToLower();
                if (wordRegex.IsMatch(word))
                {
                    if (!SpellingDictionary.dictionary.ContainsKey(word))
                        return false;
                }
            }
            return true;
        }

        public void correctPost()
        {
            Regex wordRegex = new Regex("[a-z]+", RegexOptions.IgnoreCase);
            string word, correctedWord;
            
            List<string> words = post.PostContent.Split(' ').ToList();

            foreach (var w in words)
            {
                word = w.Trim().ToLower();
                correctedWord = this.correctWord(word);
                if (word != correctedWord)
                {
                   
                    for(int i=0; i<w.Length && i<correctedWord.Length; i++)
                    {
                        if (char.IsUpper(w[i]) == true)
                            correctedWord = correctedWord.Replace(correctedWord[i], char.ToUpper(correctedWord[i])); 
                    }
                    post.PostContent = post.PostContent.Replace(w, correctedWord);
                }
            }
            
        }

        public string correctWord(string word)
        {
            if (string.IsNullOrEmpty(word))
                return word;

            if (SpellingDictionary.dictionary.ContainsKey(word))
                return word;

            List<string> edits = this.edits(word);
            Dictionary<string, int> candidates = new Dictionary<string, int>();

            foreach (string wordVariation in edits)
            {
                if (SpellingDictionary.dictionary.ContainsKey(wordVariation) && !candidates.ContainsKey(wordVariation))
                    candidates.Add(wordVariation, SpellingDictionary.dictionary[wordVariation]);
            }


            if (candidates.Count > 0)
                return candidates.OrderByDescending(x => x.Value).First().Key;

            foreach (string edit in edits)
            {
                List<string> editEdits = this.edits(edit);
                foreach(string wordVariation in editEdits)
                {
                    if (SpellingDictionary.dictionary.ContainsKey(wordVariation) && !candidates.ContainsKey(wordVariation))
                        candidates.Add(wordVariation, SpellingDictionary.dictionary[wordVariation]);
                }
            }
            if (candidates.Count > 0)
                return candidates.OrderByDescending(x => x.Value).First().Key;
            else
                return word;
        }

        private List<string> edits(string word)
        {
            var splits = new List<Tuple<string, string>>();
            var transposes = new List<string>();
            var deletes = new List<string>();
            var replaces = new List<string>();
            var inserts = new List<string>();
            string a, b;

            //Splits 
            for (int i = 0; i < word.Length; i++)
            {
                var split = new Tuple<string, string>(word.Substring(0, i), word.Substring(i));
                splits.Add(split);
            }

            //Transposes
            for (int i = 0; i < splits.Count; i++)
            {
                a = splits[i].Item1;
                b = splits[i].Item2;
                if (b.Length > 1)
                {
                    transposes.Add(a + b[1] + b[0] + b.Substring(2));
                }
            }

            //Deletes
            for (int i = 0; i < splits.Count; i++)
            {
                a = splits[i].Item1;
                b = splits[i].Item2;
                if (!string.IsNullOrEmpty(b))
                {
                    deletes.Add(a + b.Substring(1));
                }
            }

            //Replaces
            for (int i = 0; i < splits.Count; i++)
            {
                a = splits[i].Item1;
                b = splits[i].Item2;
                if (!string.IsNullOrEmpty(b))
                {
                    for (char c = 'a'; c <= 'z'; c++)
                    {
                        replaces.Add(a + c + b.Substring(1));
                    }
                }
            }

            //Inserts
            for (int i = 0; i < splits.Count; i++)
            {
                a = splits[i].Item1;
                b = splits[i].Item2;
                if (!string.IsNullOrEmpty(b))
                {
                    for (char c = 'a'; c <= 'z'; c++)
                    {
                        replaces.Add(a + c + b);
                    }
                }
            }

            return transposes.Union(deletes).Union(replaces).Union(inserts).ToList();
        }

    }
}
