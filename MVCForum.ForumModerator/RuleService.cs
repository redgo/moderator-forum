﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using MVCForum.Domain.DomainModel;
using MVCForum.Domain.Interfaces.Services;
using MVCForum.Data.Context;


namespace MVCForum.ForumModerator
{
    public class RuleService : IRuleService
    {
        private List<Rule> rules;
        private readonly string pathToXml = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Rules.xml");
		private MVCForumContext _context; 
        public RuleService()
        {
            rules = Load();
			_context = new MVCForumContext();
        }

        public IEnumerable<Rule> GetAll()
        {
            return rules;
        }

        public Rule Get(Guid id)
        {
            return GetAll().Single(x => x.Id == id);
        }

        public Rule Get(string slug)
        {
            return GetAll().Single(x => x.Name == slug);
        }

        public void Delete(Rule rule)
        {
            rules.Remove(rule);
            Save();
        }

        public void Add(Rule rule)
        {
            rules.Add(rule);
            Save();
        }

        public void Save()
        {
            using (Stream stream = File.Open(pathToXml, FileMode.Create))
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<Rule>));
                    serializer.Serialize(stream, rules);
                }
                catch (Exception)
                {
                    Console.WriteLine("Error during serialization");
                }
            }
        }

        public List<Rule> Load()
        {
            using (Stream stream = File.Open(pathToXml, FileMode.Open))
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<Rule>));
                    return (List<Rule>)serializer.Deserialize(stream);
                }
                catch (Exception)
                {
                    Console.WriteLine("Error during deserialization");
                    return new List<Rule>();
                }
            }
        }

        public void UpdateSlugFromName(Rule rule)
        {
            rules.Remove(rule);
            rules.Add(rule);
            Save();
        }

		public void MessageAllThatRulesHaveChanged(string UserId)
		{
			//var usersToInform = _context.MembershipUser.Where()
		}
    }
}
