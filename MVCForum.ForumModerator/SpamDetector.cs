﻿using Expat.Bayesian;
using MVCForum.Data.Context;
using MVCForum.Data.Repositories;
using MVCForum.Domain.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace MVCForum.ForumModerator
{
	public class SpamDetector
	{
		public bool DetectIfPostHaveSpam(Post post)
		{
			var isSpam = new SpamFilter().Test(post.PostContent);
			return isSpam > 0.9 ? true : false;
		}

		public void DeletePostWithSpam(Post post)
		{
			using (var context = new MVCForumContext())
			{
				var postRep = new PostRepository(context);
				postRep.Delete(post);
			}
			
		}
	}
}
