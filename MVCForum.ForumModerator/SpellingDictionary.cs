﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MVCForum.ForumModerator
{
    class SpellingDictionary
    {
        public static Dictionary<String, int> dictionary { get; set; }

        static SpellingDictionary()
        {
            dictionary = new Dictionary<string, int>();
            Regex wordRegex = new Regex("[a-z]+");
            string word;
            
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\MVCForum.ForumModerator\dictionary.txt");
            string fileDictionary = File.ReadAllText(path,UTF8Encoding.UTF8);
            
            List<string> words = fileDictionary.Split(' ').ToList();
            
            foreach (var w in words)
            {
                word = w.Trim(new Char[] { ' ', '*', '.', ',' }).ToLower();
                if (wordRegex.IsMatch(word))
                {
                    if (dictionary.ContainsKey(word))
                        dictionary[word]++;
                    else
                        dictionary.Add(word, 1);
                }
            }
        }
    }
}
