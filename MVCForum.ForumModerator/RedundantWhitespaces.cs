﻿using MVCForum.Domain.DomainModel;
using System.Text.RegularExpressions;

namespace MVCForum.ForumModerator
{
    public class RedundantWhiteSpaces
    {
        public Post post { get; set; }

        public RedundantWhiteSpaces(Post post)
        {
            this.post = post;
        }

        public bool isValid()
        {
            bool isValid = this.checkSpaces() && this.checkTabs() && this.checkVerticalTabs() && this.checkNewLines();
            return isValid;
        }

        public bool checkSpaces()
        {
            Regex regex = new Regex("  +");
            bool isValid = !regex.IsMatch(post.PostContent);
            return isValid;
        }

        public bool checkTabs()
        {
            Regex regex = new Regex("\t\t+");
            bool isValid = !regex.IsMatch(post.PostContent);
            return isValid;
        }

        public bool checkVerticalTabs()
        {
            Regex regex = new Regex("\v\v+");
            bool isValid = !regex.IsMatch(post.PostContent);
            return isValid;
        }

        public bool checkNewLines()
        {
            Regex regex = new Regex("\r\n\r\n(\r\n)+");
            bool isValid = !regex.IsMatch(post.PostContent);
            return isValid;
        }

        public void correctPost()
        {
            this.deleteSpaces();
            this.deleteTabs();
            this.deleteVerticalTabs();
            this.deleteNewLines();
        }

        public void deleteSpaces()
        {
           Regex regex = new Regex(" +");
           post.PostContent = regex.Replace(post.PostContent, " ");	           
        }

        public void deleteTabs()
        {
            Regex regex = new Regex("\t+");
            post.PostContent = regex.Replace(post.PostContent, "\t");
        }

        public void deleteVerticalTabs()
        {
            Regex regex = new Regex("\v+");
            post.PostContent = regex.Replace(post.PostContent, "\v");
        }

        public void deleteNewLines()
        {
            Regex regex = new Regex("\r\n(\r\n)+");
            post.PostContent = regex.Replace(post.PostContent, "\r\n\r\n");
        }
    }
}
