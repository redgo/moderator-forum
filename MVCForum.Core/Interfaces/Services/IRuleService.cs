﻿using System;
using System.Collections.Generic;
using MVCForum.Domain.DomainModel;

namespace MVCForum.Domain.Interfaces.Services
{
    public interface IRuleService
    {
        IEnumerable<Rule> GetAll();
        Rule Get(Guid id);
        Rule Get(string slug);
        void Delete(Rule rule);
        void Add(Rule rule);
        void Save();
        void UpdateSlugFromName(Rule rule);
    }
}
