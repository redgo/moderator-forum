﻿using MVCForum.Domain.DomainModel;
using MVCForum.Domain.Interfaces.Events;

namespace MVCForum.Domain.Events
{
    public class PostMadeEventArgs : MVCForumEventArgs
    {
		private Post _post;
		public Post Post
		{
			get
			{
				return _post;
			}
			set
			{
				_post = value;
			}
		}
	}
}
