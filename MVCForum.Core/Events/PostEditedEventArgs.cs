﻿using MVCForum.Domain.DomainModel;
using MVCForum.Domain.Interfaces.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCForum.Domain.Events
{
	public class PostEditedEventArgs : MVCForumEventArgs
	{
		private Post _post;
		public Post Post
		{
			get
			{
				return _post;
			}
			set
			{
				_post = value;
			}
		}
	}
}
