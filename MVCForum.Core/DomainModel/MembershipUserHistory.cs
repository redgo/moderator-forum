﻿using MVCForum.Utilities;
using System;

namespace MVCForum.Domain.DomainModel
{
	public class MembershipUserHistory : Entity
	{
		public MembershipUserHistory()
        {
            Id = GuidComb.GenerateComb();
        }

		public Guid Id { get; set; }

		public Guid MemberId { get; set; }
		public DateTime WarningDate { get; set; }
		public String CreatedBy { get; set; }
		public String Reason { get; set; }

		public virtual MembershipUser MembershipUser { get; set; }
	}
}
