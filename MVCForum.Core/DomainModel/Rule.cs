﻿using System;
using System.Collections.Generic;

namespace MVCForum.Domain.DomainModel
{
    public enum PenaltyKind
    {
        Warning,
        Ban
    }

    public enum Result
    {
        Edit,
        Remove
    }

    public enum BasicRule
    {
        RemoveWhitespaces,
        Curses,
        Pictures,
        Spam,
        Spelling
    }

    public class Penalty
    {
        public PenaltyKind Kind { get; set; }
        public int LengthInDays { get; set; }
    }


    public class ActionsToMade
    {
        public BasicRule Key { get; set; }
        public bool Value { get; set; }
    }

    public class Rule
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool NeedConfirmation { get; set; }
        public Penalty Penalty { get; set; }
        public Result Result { get; set; }
        public string EditMessage { get; set; }
        public List<ActionsToMade> ActionsToMade { get; set; }
    }
}
